var JSONData = [
    {
        "Role": "Software Developers & Programmers",
        "Seek": 590,
        "SalaryMax": 100,
        "SalaryMin": 72,
        "Skills": ["Computer software and systems", "programming languages and techniques", "software development processes such as Agile", "confidentiality, data security and data protection issues."],
        "Description": "Software developers and programmers develop and maintain computer software, websites and software applications (apps).",
        "Image": "softwareDeveloper.jpg"
    },
    {
        "Role": "Databases and Systems",
        "Seek": 74,
        "SalaryMax": 90,
        "SalaryMin": 66,
        "Skills": ["A range of database technologies and operating systems", "new developments in databases and security systems", "computer and database principles and protocols"],
        "Description": "Databases & systems administrators develop, maintain and administer computer operating systems, database management systems, and security policies and procedures",
        "Image": "Database.jpg"
    },
    {
        "Role": "Help Desk & IT Support",
        "Seek": 143,
        "SalaryMax": 65,
        "SalaryMin": 46,
        "Skills": ["computer hardware, software, networks and websites", "the latest developments in information technology"],
        "Description": "Information technology (IT) helpdesk/support technicians set up computer and other IT equipment and help prevent, identify and fix problems with IT hardware and software.",
        "Image": "helpDesk.jpg"
    },
    {
        "Role": "Data Analyst",
        "Seek": 270,
        "SalaryMax": 128,
        "SalaryMin": 69,
        "Skills": ["data analysis tools such as Excel, SQL, SAP and Oracle, SAS or R", "data analysis, mapping and modelling techniques", "analytical techniques such as data mining"],
        "Description": "Data analysts identify and communicate trends in data using statistics and specialised software to help organisations achieve their business aims.",
        "Image": "dataAnalyst.jpg"
    },
    {
        "Role": "Test Analyst",
        "Seek": 127,
        "SalaryMax": 98,
        "SalaryMin": 70,
        "Skills": ["programming methods and technology", "computer software and systems"],
        "Description": "Test analysts design and carry out testing processes for new and upgraded computer software and systems, analyse the results, and identify and report problems.",
        "Image": "testAnalyst.jpg"
    },
    {
        "Role": "Project Management",
        "Seek": 188,
        "SalaryMax": 190,
        "SalaryMin": 110,
        "Skills": ["principles of project management", "approaches and techniques such as Kanban and continuous testing", "how to handle software development issues", "common web technologies used by the scrum team."],
        "Description": "Project managers use various methods to keep project teams on track. They also help remove obstacles to progress.",
        "Image": "projectManagement.jpg"
    }

];


function change(arg) {

    console.log(arg);
    var name = arg.id;
    var number = name.substr(5);
    $('#flexHeading').html(JSONData[number]["Role"]);
    $('#flexText').html(JSONData[number]["Description"]);
    loadList(number);
    //Images need to be stored in "../images/" folder
    $('#flex-image').html('<img src="../images/' + JSONData[number]["Image"] + '">');


}

function loadTable() {

    //Finds table html tag and creates a header row with headings in headingArray
    var myTable = document.getElementById("jobTable");
    var head = document.createElement("tr");
    myTable.appendChild(head);

    var headingArray = ["Role", "Job Vacancies 08/2108", "Salary Max", "Salary Min"]
    cellFunction(headingArray, "th", head);

    var totalVacancies = 0;
    var totalMax = 0;
    var totalMin = 0;

    //Goes through JSON array, for each entry create a new row
    //The cell values are taken from the JSON table using the keyArray
    for (var i = 0; i < JSONData.length; i++) {

        var row = document.createElement("tr");
        var rowName = "Entry" + i;

        $(row).attr('id', rowName);
        $(row).attr('onClick', 'change(this)');
        myTable.appendChild(row);
        var keyArray = [JSONData[i]["Role"], JSONData[i]["Seek"], JSONData[i]["SalaryMax"], JSONData[i]["SalaryMin"]];
        cellFunction(keyArray, "td", row);


        //get totals for total and average cells at end of table
        totalVacancies = totalVacancies + parseInt(JSONData[i]["Seek"]);
        totalMax = totalMax + parseInt(JSONData[i]["SalaryMax"]);
        totalMin = totalMin + parseInt(JSONData[i]["SalaryMin"]);
    }

    var statsrow = document.createElement("tr");
    var rowName = "Statistics";

    $(statsrow).attr('id', rowName);

    myTable.appendChild(statsrow);
    var statsArray = ["", "Total: " + totalVacancies, "Average: " + Math.round((totalMax / JSONData.length) * 100) / 100, "Average: " + Math.round((totalMin / JSONData.length) * 100) / 100];
    cellFunction(statsArray, "td", statsrow);


}

//presents skills data as a list
function loadList(number) {
    var listNode = document.getElementById("flexList");
    var array = JSONData[number]["Skills"];

    listNode.innerHTML = null; //delete existing list if there is one, otherwise it will add to it
    for (var i = 0; i < array.length; i++) {
        var listItem = document.createElement("li");
        listItem.innerHTML = array[i];
        listNode.appendChild(listItem);
    }
}

//A function that creates cells(tag) for a table row(parent) and sets the values using data from an array (array)
function cellFunction(array, tag, Parent) {
    for (var j = 0; j < array.length; j++) {
        var cell = document.createElement(tag);
        Parent.appendChild(cell);
        cell.innerHTML = array[j];
    }
}

//javascript calls table function once page has loaded
window.onload = function () {

    loadTable();
}


